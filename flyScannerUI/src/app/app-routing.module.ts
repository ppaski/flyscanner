import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlightSearchComponent } from './flight-search/flight-search.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { ItalyComponent } from './italy/italy.component';
import { SpainComponent } from './spain/spain.component';
import { PopularDirectionsComponent } from './popular-directions/popular-directions.component';
import { AboutProjectComponent } from './about-project/about-project.component'

const routes: Routes = [
  { path: '', component: FlightSearchComponent },
  { path: 'search', component: FlightSearchComponent },
  { path: 'profile', component: MyProfileComponent },
  { path: 'italy', component: ItalyComponent },
  { path: 'spain', component: SpainComponent },
  { path: 'popularDirections', component: PopularDirectionsComponent },
  { path: 'aboutProject', component: AboutProjectComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
