import { Injectable } from '@angular/core';
import {IFLightProvider, SearchResult} from './IFLightProvider'

@Injectable({
    providedIn: 'root'
})
export class DummySearchProvider implements IFLightProvider{
    SearchFlights(departureDate: Date): SearchResult[]{
        return this.GetFlightsByDate(departureDate);
    }

    // SearchFlightsWithReturn(departureDate: Date, returnDate: Date): SearchResult[]{
    //     var results: SearchResult[];
    //     results.concat(this.GetFlightsByDate(departureDate), this.GetFlightsByDate(returnDate));

    //     return results;
    // }

    GetFlightsByDate(departureDate: Date): SearchResult[]{
        let resultsNumber = Math.floor(Math.random() * 5) + 1;
        let dummyDepartureHour = 0;
        var results: SearchResult[] = [];
        for(var i = 0; i < resultsNumber; i++){
            var result = <SearchResult>{};
            dummyDepartureHour += 2;
            let durationHours = Math.floor(Math.random() * 7) + 1;
            
            result.departure = new Date(departureDate);
            result.departure.setHours(dummyDepartureHour, 0, 0);
            result.arrival = new Date(departureDate);
            result.arrival.setHours(dummyDepartureHour + durationHours, 0, 0);
            result.duration = durationHours;
            result.price = Math.floor(Math.random() * 1000) + 1;
            results;
            results.push(result);
        }

        return results;
    }

    SearchFlightsWithReturn(departureDate: Date, arrivalDate: Date): SearchResult[]{
        let resultsNumber = Math.floor(Math.random() * 5) + 1;
        let dummyDepartureHour = 0;
        var results: SearchResult[] = [];
        for(var i = 0; i < resultsNumber; i++){
            var result = <SearchResult>{};
            dummyDepartureHour += 2;
            let durationHours = Math.floor(Math.random() * 7) + 1;
            
            result.departure = new Date(departureDate);
            result.departure.setHours(dummyDepartureHour, 0, 0);
            result.arrival = new Date(arrivalDate);
            result.arrival.setHours(dummyDepartureHour + durationHours, 0, 0);
            result.duration = durationHours;
            result.price = Math.floor(Math.random() * 1000) + 1;
            results;
            results.push(result);
        }

        return results;
    }
}