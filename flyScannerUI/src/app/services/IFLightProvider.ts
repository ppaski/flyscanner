import { Injectable } from '@angular/core';

@Injectable()
export abstract class IFLightProvider {
    abstract SearchFlights(departureDate: Date): SearchResult[];

    abstract SearchFlightsWithReturn(departureDate: Date, returnDate: Date): SearchResult[];
}

export interface SearchResult{
    departure: Date;
    arrival: Date;
    duration: Number;
    price: Number;
}