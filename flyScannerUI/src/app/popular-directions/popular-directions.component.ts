import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-popular-directions',
  templateUrl: './popular-directions.component.html',
  styleUrls: ['./popular-directions.component.css']
})
export class PopularDirectionsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  @HostListener('wheel', ['$event'])
  onWheelScroll(evento: WheelEvent) {
    // Scroll down
    console.log("Event")
    if (evento.deltaY > 0) {
      this.router.navigate(['spain'])
    }
  }
 countries = [
  "assets/img/Italy.jpg",
  "assets/img/Spain.jpg"
];

}
