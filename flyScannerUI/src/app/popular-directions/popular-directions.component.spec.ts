import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularDirectionsComponent } from './popular-directions.component';

describe('PopularDirectionsComponent', () => {
  let component: PopularDirectionsComponent;
  let fixture: ComponentFixture<PopularDirectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularDirectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularDirectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
