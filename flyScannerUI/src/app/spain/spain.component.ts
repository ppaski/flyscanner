import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-spain',
  templateUrl: './spain.component.html',
  styleUrls: ['./spain.component.css']
})
export class SpainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  @HostListener('wheel', ['$event'])
  onWheelScroll(evento: WheelEvent) {
    // Scroll down
    if (evento.deltaY < 0) {
      this.router.navigate(['popularDirections'])
    }
    else
    {
      if ((window.innerHeight + window.scrollY) >= (document.documentElement.scrollHeight -1)){
        this.router.navigate(['italy'])
      }
      else if (window.innerHeight == document.documentElement.scrollHeight){
        this.router.navigate(['italy'])
      }
    }
  }

  public mapOfSpainPicture = "assets/img/map_of_spain.jpg";
  public flagOfSpain = "assets/img/Flag_of_Spain.png";
  public photos = [
    "assets/img/spain/s1.jpg",
    "assets/img/spain/s3.jpg",
    "assets/img/spain/s4.jpg",
    "assets/img/spain/s5.jpg",
    "assets/img/spain/s6.jpg",
  ];
}
