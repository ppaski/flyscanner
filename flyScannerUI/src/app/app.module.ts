import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlightSearchComponent } from './flight-search/flight-search.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopularDirectionsComponent } from './popular-directions/popular-directions.component';
import { SpainComponent } from './spain/spain.component';
import { ItalyComponent } from './italy/italy.component';
import { SliderModule } from 'angular-image-slider';
import { AboutProjectComponent } from './about-project/about-project.component';



@NgModule({
  declarations: [
    AppComponent,
    FlightSearchComponent,
    MyProfileComponent,
    PopularDirectionsComponent,
    SpainComponent,
    ItalyComponent,
    AboutProjectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserModule, 
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbDropdownModule,
    NgbModule,
    SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
