import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-italy',
  templateUrl: './italy.component.html',
  styleUrls: ['./italy.component.css']
})
export class ItalyComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

    @HostListener('wheel', ['$event'])
  onWheelScroll(evento: WheelEvent) {
    // Scroll up
    console.log("Event")
    if (evento.deltaY < 0) {
      this.router.navigate(['spain'])
    }
  }

  public mapOfItaly = "assets/img/italy/map_of_italy.jpg";
  public flagOfItaly = "assets/img/flag_of_italy.png";
  public photos = [
    "assets/img/italy/1.jpg",
    "assets/img/italy/2.jpg",
    "assets/img/italy/3.jpg",
    "assets/img/italy/4.jpg",
    "assets/img/italy/5.jpg",
    "assets/img/italy/6.jpg",
  ];

}
