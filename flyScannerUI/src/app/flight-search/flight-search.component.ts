import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DummySearchProvider } from '../services/DummySearchProvider';
import { SearchResult } from '../services/IFLightProvider';

@Component({
  selector: 'app-flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.css']
})
export class FlightSearchComponent implements OnInit {

  results: SearchResult[];
  returnResults: SearchResult[];
  searchResultsVisibe: boolean;
  public dateTime: Date;
  public clicked: boolean;
  directions: string;
  public isRoundTrip: boolean;
  public searchedDeparturePlace: string;
  public searchedArrivalPlace: string;

  flightForm = new FormGroup({
    departurePlace: new FormControl('', [Validators.required]),
    destinationPlace: new FormControl('', [Validators.required]),
    departureDate: new FormControl(new Date(), [Validators.required]),
    returnDate: new FormControl(new Date(), [Validators.required])
  }, this.datesAreValid)

  constructor(private searchProvider: DummySearchProvider) {
    this.directions = "Pick direction";
    this.searchResultsVisibe = false;
    this.isRoundTrip = false;
  };

  get departurePlace(): any {
    return this.flightForm.get('departurePlace');
  }

  get destinationPlace(): any {
    return this.flightForm.get('destinationPlace');
  }

  datesAreValid(group: FormControl)
  {
    var departureDate = group.get('departureDate');
    var returnDate = group.get('returnDate');
    var depDate = new Date(departureDate.value);
    var retDate = new Date(returnDate.value)
    var isValid = retDate.getDate() <= depDate.getDate();

    if (isValid)
    {
      return null;
    }

    return  {valid: false};
  }

  ngOnInit() {
  }

  onDirectionClicked(value)
  {
    this.directions = value.srcElement.innerText;
    if (this.directions == "Round trip"){
      this.isRoundTrip = true;
    }
    else{
      this.isRoundTrip = false;
    }
  }

  onSwitchClicked(){
    let temp = this.flightForm.get("departurePlace").value;
    this.flightForm.get("departurePlace").setValue(this.flightForm.get("destinationPlace").value);
    console.log(temp);
    this.flightForm.get("destinationPlace").setValue(temp);
  }

  search(){
    var depPlace = this.flightForm.get("departurePlace");
    this.clicked = true;
    
    if (!this.isFormValid())
    {
      console.log("Form not valid");
      return;
    }

    this.searchedDeparturePlace = this.departurePlace.value;
    this.searchedArrivalPlace = this.destinationPlace.value;
    this.searchResultsVisibe = true;
    var departureDate = this.flightForm.get('departureDate');
    var depDate = new Date(departureDate.value);
    this.results = this.searchProvider.SearchFlights(depDate);

    if (this.isRoundTrip){
      var returnDate = this.flightForm.get('returnDate');
      var retDate = new Date(returnDate.value)
      this.returnResults = this.searchProvider.SearchFlights(retDate);
    }
  }

  private isFormValid()
  {
    var depPlace = this.flightForm.get("departurePlace");
    var destPlace = this.flightForm.get("destinationPlace");
    var depDate= this.flightForm.get("departureDate");
    var retDate = this.flightForm.get("returnDate");

    return depPlace.valid && destPlace.valid && depDate.valid && retDate.valid && this.flightForm.valid;
  }
}
